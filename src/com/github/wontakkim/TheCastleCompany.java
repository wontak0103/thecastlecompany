package com.github.wontakkim;

public class TheCastleCompany {

    enum Slope {
        ASCENT,
        DESCENT,
        FLAT,
    }

    public static void main(String[] agrs) {
        int[][] lands = {
                null,
                { },
                { 1 },
                { 1, 1, 1, 1, 1 },
                { 1, 2, 2, 2, 1 },
                { 1, 2, 1, 2, 1 },
                { 1, 1, 2, 2, 1, 1, 2, 2 },
                { 1, 2, 6, 4, 3, 2, 1, 5, 3, 6, 4, 5, 3, 3, 4, 5 },
        };

        for (int[] land : lands) {
            print(land, getCastleCount(land));
        }
    }

    public static int getCastleCount(int[] land) {
        if (land == null || land.length == 0) {
            return 0;
        }

        int count = 1;
        Slope oldSlope = Slope.FLAT;

        for (int i=0; i<land.length-1; i++) {
            Slope slope = getSlope(land[i], land[i+1]);
            if (slope == Slope.FLAT) {
                continue;
            }

            if (oldSlope != Slope.FLAT && oldSlope != slope) {
                count ++;
            }

            oldSlope = slope;
        }

        return count;
    }

    public static Slope getSlope(int prev, int next) {
        if (prev == next) {
            return Slope.FLAT;
        }
        return prev < next ? Slope.ASCENT : Slope.DESCENT;
    }

    public static void print(int[] land, int castleCount) {
        System.out.print("{");

        if (land != null) {
            for (int i=0; i<land.length; i++) {
                if (i != 0) {
                    System.out.print(", ");
                }

                System.out.print(land[i]);
            }
        } else {
            System.out.print("null");
        }

        System.out.print("}");

        System.out.printf(" Castle Count : %d\n", castleCount);
    }
}
